# README #

To get started you can fork and clone the repository from bitbucket.

You need to make sure you have node installed on your computer. To check this you can type in node -v in the command line.

### What is this repository for? ###

* This application prints out the number of basket and the total number of items in a basket

### How do I get set up? ###

* Once you have forked and cloned the project, in the command line 'cd' into the project and type 'npm install' to install all dependencies.
* You will need 3 command lines to run this project. Make sure you are in the grocery directory in all 3.
* In the first command line type 'npm run seed'. This will seed the database. Then type 'mongod'.
* In the second command line type 'npm run dev:server' This will start the server. You will be able to view the server in a browser by typing in localhost:3000/api/grocery
* In the third command line type 'npm run dev:front'. This will start the React application You will be able to view the information in a second browser type in localhost: 9090
* In order to view the test. Firstly you need to cancel the server by pressing ctrl c in the second command line. Then you can type 'npm test' to view the unit tests for the server routes.

### Contribution guidelines ###

* Expressjs
* Nodejs
* React
