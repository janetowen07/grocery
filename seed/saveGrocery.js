const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Grocery = require('../models/grocery');

function saveGrocery(grocery) {
  return mongoose.connection.dropDatabase().then(() => {
    grocery = grocery.map(item => {
      const fruit = new Grocery(item);
      return fruit.save();
    });
    return Promise.all(grocery);
  });
}

module.exports = saveGrocery;
