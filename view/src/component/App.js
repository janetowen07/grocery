import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

const path = 'http://localhost:3000/api/grocery';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fruit: []
    };
  }
  
  componentDidMount() {
    axios.get(path)
      .then(res => {
          console.log(res.data);
        this.setState({fruit: res.data});
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render () {
    return (
      <div>
        <h3 className='title is-3'>Shopping List</h3>
        {this.state.fruit.map((elem) => {
          console.log(elem);
          return (<div key = {elem._id}>
            Name = {elem.name} <br/>
            Number of Basket = {elem.baskets.length} <br />
            Basket = {elem.baskets.reduce((item, acc) => {return item + acc;},0)} <br/><br/>
          </div>);
        })}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.any
};

export default App;
