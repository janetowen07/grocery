const saveGrocery = require('./saveGrocery');
const data = require('./groceryData');
const mongoose = require('mongoose');
const DB = 'mongodb://localhost/grocery';

mongoose.connect(DB);

saveGrocery(data)
  .then(() => {
    console.log('Groceries seeded successfully in ' + DB);
  })
  .catch(err => {
    console.log('Error seeding database', err);
  })
  .then(() => {
    mongoose.connection.close();
  });