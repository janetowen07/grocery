const express = require('express');
const Grocery = require('../models/grocery');
const router = express.Router();

router.get('/grocery', (req, res) => {
    Grocery.find()
    .then((item) => {
            res.status(200).json(item);
        })
        .catch((err) => {
            res.status(404).json(err);
        });
});

module.exports = router;