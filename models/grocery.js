const mongoose = require('mongoose');

const grocerySchema = new mongoose.Schema({ 
    name: {
        type: String,
        required: true
    },
    baskets: {
        type: Array,
        required: true
    },
    unit_cost: {
        type: Number,
    }
});

module.exports = mongoose.model('Grocery', grocerySchema);