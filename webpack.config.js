const path = require('path');

module.exports = {
  entry: './view/src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'view', 'public', 'js'),
    publicPath: '/'
  },
  devtool: 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, 'view/src'),
        loader: 'babel-loader',
        options: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader'
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'view', 'public'),
    publicPath: '/js/',
    port: 9090
  }
};