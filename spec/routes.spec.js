const { expect } = require('chai');
const request = require('supertest');
const mongoose = require('mongoose');
const server = require('../server');
const data = require('../seed/groceryData');
const saveGrocery = require('../seed/saveGrocery');

describe('API Route', function() {
  beforeEach(done => {
    saveGrocery(data)
      .then(() => done())
      .catch(err => done(err));
  });
  after(done => {
    mongoose.connection.close();
    done();
	});
	
	describe('GET /grocery', function () {
		it('returns', function (done) {
			request(server)
				.get('/api/grocery')
				.end((err, res) => {
					if (err) done(err);
					else {
						expect(res.statusCode).to.equal(200);
						expect(res.body).to.be.an('array');
						expect(res.body[0].name).to.equal('apples');
						done();
					}
				});
		});
	});
});
