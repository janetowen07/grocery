const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();

const router = require('./routes/index');
const bodyParser = require('body-parser');

const dbURI = 'mongodb://localhost:27017/grocery';

mongoose.connect(dbURI, (err) => {
    if (err) return console.log(err);
    console.log('Connected to DB');
});

app.use(cors());
app.use('/api', router);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const PORT = process.env.PORT || 3000;
app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
});

module.exports = app;